/**
 * Creates redux store
 * @param  {function} reducer
 * @return {object}   redux store
 */
module.exports.createStore = (reducer) => {
  let state;
  let listeners = [];

  const getState = () => state;

  const dispatch = (action) => {
    state = reducer(state, action);
    listeners.forEach(listener => listener());
  };

  const subscribe = (listener) => {
    listeners.push(listener);
    return () => {
      listeners = listeners.filter(l => l !== listener);
    };
  };

  dispatch({});

  return { getState, dispatch, subscribe };
}

/**
 * Redux reducer
 * @param  {Number} state
 * @param  {string} action
 * @return {Number} new state
 *
const counter = (state = 0, action) => {
  switch(action.type){
    case 'INCREMENT':
     return state + 1;
    case 'DECREMENT':
     return state - 1;
    default:
     return state;
  }
}
/**/

const store = createStore(counter);

// console.log(store.getState());
// store.dispatch({ type: 'INCREMENT' });
// console.log(store.getState());

const _render = () => {
  document.body.innerHTML = store.getState();
}

store.subscribe(_render);
_render();

document.body.addEventListener('click', () => {
  store.dispatch({ type: 'INCREMENT' });
})