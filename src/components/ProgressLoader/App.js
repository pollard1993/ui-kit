/**
 * Dependencies
 */
import React from 'react';
import PropTypes from 'prop-types';
import ProgressLoader from './index';

/**
 * Dev
 */
import Images from '../../utils/dev/images';

export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.setState({
      items: Images(100),
    });
  }


  render() {
    return (
      <div>
        <ProgressLoader items={this.state.items} />
      </div>
    );
  }
}