/**
 * Dependencies
 */
import React from 'react';
import PropTypes from 'prop-types';
import {Motion, spring} from 'react-motion';
import axios from 'axios';

/**
 * ProgressLoader
 */
class ProgressLoader extends React.Component{

  constructor(props){
    super(props);

    this.state = {
      prog: 0,
    };

    this.load(props.items);
  }


  /**
   * Load assets via axios, update on every promise
   * @param  {array} assets - urls for xhr
   */
  load(assets){
    let promises   = [];
    let loaded     = [];
    let load_count = 0;
    let load_total = assets.length - 1;
    let time_start = new Date().getTime();

    // Create asset promises
    promises = assets.map(url => {
      return new Promise((resolve, reject) => {
        axios.get(url).then(response => {
          // Update progress info
          load_count++;
          loaded.push(url);

          this.setState({
            ...this.state.prog,
            just_loaded: url,
            loaded: loaded,
            load_count: load_count,
            load_total: load_total,
            prog: load_count / load_total,
          });
          resolve(url);
        }).catch(error => {
          reject(url);
        });
      });
    });

    // Group resolver
    axios.all(promises).then(response => {
      // console.log("response", response);
      this.setState({
        just_loaded: null,
        duration: new Date().getTime() - time_start,
        loaded: loaded,
        load_count: load_count,
        load_total: load_total,
        prog: 1,
      });
    }).catch(failed => {
      // console.log("failed", failed);
      this.setState({
        just_loaded: null,
        failed: failed,
        loaded: loaded,
        load_count: load_count,
        load_total: load_total,
        prog: load_count / load_total,
      });
    });
  }


  render(){
    // console.log(this.state.prog);
    return (
      <div
        className='progress-loader-wrap'
      >
        <Motion
          defaultStyle={{
            prog: 0,
          }}
          style={{
            prog: spring(this.state.prog),
          }}
        >
          {({prog}) =>
            <svg
              viewBox="0 0 100 100"
              xmlns="http://www.w3.org/2000/svg"
              className='progress-loader'
            >
              <path
                d={`
                  M50,5
                  a45,45 0 1,0 0, 90
                  a-45,-45 0 1,0 0, -90
                `}
                ref={path => {
                  if(!this.path){
                    this.path = path;
                    // Set length of path
                    this.length = this.path.getTotalLength();
                  }
                }}
                style={{
                  strokeDashoffset: this.length * (1 - prog),
                  opacity: prog,
                  fill: 'none',
                  stroke: '#000',
                  strokeDasharray: this.length,
                  strokeLinecap: 'round',
                  strokeWidth: 10,
                }}
              />
              <text x="50%" y="50%" text-anchor="middle" alignment-baseline="central">{new Number(prog * 100).toFixed(0)}%</text>
            </svg>
          }
        </Motion>
      </div>
    );
  }

}

ProgressLoader.propTypes = {
  items: PropTypes.array,
};


export default ProgressLoader;