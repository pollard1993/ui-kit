/**
 * Dependencies
 */
import React from 'react';
import PropTypes from 'prop-types';
import TextBlur from './index';

/**
 * Dev Dependencies
 */
import Images from '../../utils/dev/images';

export default class App extends React.Component {
  constructor(props){
    super(props);

    this.items = [
      {
        text: 'Carousel',
        image: Images(1, 'Carousel')[0],
        deviation: 5,
        href: '/Carousel/',
      },
      {
        text: 'Lightbox',
        image: Images(1, 'Lightbox')[0],
        deviation: 5,
        href: '/Lightbox/',
      },
      {
        text: 'PlayPause',
        image: Images(1, 'pause')[0],
        deviation: 5,
        href: '/PlayPause/',
      },
      {
        text: 'Progress',
        image: Images(1, 'progress')[0],
        deviation: 5,
        href: '/Progress/',
      },
      {
        text: 'Toast',
        image: Images(1, 'Toast')[0],
        deviation: 5,
        href: '/Toast/',
      },
    ];
  }


  render(){
    return (
      <div>
        {
          this.items.map((item, i) => {
            return(
              <a className='text-wrap' href={`${window.location.origin}${item.href}`}>
                <TextBlur
                  text={item.text}
                  image={item.image}
                  deviation={item.deviation}
                  _key={i}
                />
              </a>
            )
          })
        }
      </div>
    );
  }
}