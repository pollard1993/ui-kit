/**
 * Dependencies
 */
import React from 'react';
import PropTypes from 'prop-types';


/**
 * TextBlur
 */
class TextBlur extends React.Component{

  constructor(props){
    super(props);
  }


  render(){
    return (
      <div>
        <span
          style={{
            backgroundImage: `url('${this.props.image}')`,
          }}
        />
        <svg
          viewBox="0 0 500 250"
          preserveAspectRatio="xMidYMid slice"
        >
          <defs>
            <filter id={`blur-${this.props._key}`}>
                <feGaussianBlur stdDeviation={this.props.deviation} />
            </filter>
          </defs>

          <pattern
            id={this.props._key}
            viewBox="0 0 500 250"
            patternUnits="userSpaceOnUse"
            x="0"
            y="0"
            width="100%"
            height="100%"
          >
            <image
              id="svg-image"
              xlinkHref={this.props.image}
              width="100%"
              height="100%"
              style={`filter:url(#blur-${this.props._key});`}
              preserveAspectRatio="xMidYMid slice"
            />
          </pattern>
          <text
            text-anchor="middle"
            width="50%"
            x="50%"
            y="50%"
            alignmentBaseline="central"
            class="img-layer"
            style={{
              fill: `url(#${this.props._key})`
            }}
           >
            {this.props.text}
          </text>
        </svg>
      </div>
    );
  }

}

TextBlur.propTypes = {
  text: PropTypes.string,
  image: PropTypes.string,
  deviation: PropTypes.number,
  href: PropTypes.string,
};


export default TextBlur;