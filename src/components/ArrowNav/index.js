/**
 * Dependencies
 */
import React from 'react';
import PropTypes from 'prop-types';
require('./scss/main.scss');

/**
 * ArrowNav
 */
class ArrowNav extends React.Component{

  constructor(props){
    super(props);

    this._handleLeft  = props.left;
    this._handleRight = props.right;
  }


  render(){
    return (
      <div className='arrow-nav'>
        <div 
          className='arrow left' 
          onClick={this._handleLeft}
        />
        <div 
          className='arrow right' 
          onClick={this._handleRight}
        />
      </div>
    );
  }

}

ArrowNav.propTypes = {
  left: PropTypes.func,
  right: PropTypes.func,
};


export default ArrowNav;