/**
 * Run on full page load
 */
const windowLoad = () => {
  window.removeEventListener('load', windowLoad);
  console.info('Window loaded');

  // Require new App
  require('./App')();
}

// Load
window.addEventListener('load', windowLoad);

let domReady = () => {
  document.removeEventListener('DOMContentLoaded', domReady, false);
  console.info('Document ready');
}

// Ready
document.addEventListener('DOMContentLoaded', domReady);

// Webpack Hot Module Replacement API
/**/
if(module.hot){
  module.hot.accept('./App', windowLoad);
}
/**/