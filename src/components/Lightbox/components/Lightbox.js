const youtubeEmbedData = require(`${CONSTANTS.UTILS_DIR}/data/youtube-embed-data`);
const fade             = require(`${CONSTANTS.UTILS_DIR}/animation/fade`);

/**
 * EC LIGHTBOX - EXAMPLES
 *
 *  -- VIDEO ELEMENT --
 *
 *  href (required) - link to video
 *  data-'lightbox'="video" (required)
 *  data-video-size='{"width":640, "height":390}' (optional - falls back to 1200x675) must be 16:9
 *
 *  <a href="https://www.youtube.com/watch?v=bdnHKdb-Oss" data-lightbox="video" data-video-size='{"width":640, "height":390}'>
 *    <img src="https://images.unsplash.com/photo-1465126188505-4f04a0f23a4d?dpr=1&auto=compress,format&fit=crop&w=800&h=800&q=80&cs=tinysrgb&crop=">
 *  </a>
 *
 *  -- IMAGE ELEMENT --
 *
 *  href (required) - image link
 *  data-lightbox (required)
 *
 *  <a href="https://images.unsplash.com/photo-1475518845976-0fd87b7e4e5d?dpr=1&auto=compress,format&fit=crop&w=800&h=800&q=80&cs=tinysrgb&crop=" data-lightbox>
 *    <img src="https://images.unsplash.com/photo-1475518845976-0fd87b7e4e5d?dpr=1&auto=compress,format&fit=crop&w=800&h=800&q=80&cs=tinysrgb&crop=">
 *  </a>
 *
 *  -- INIT --
 *
 *  new Lightbox();
 *
 */

module.exports = class Lightbox{

  /**
   * Constructs the prototype
   * @param  {[object]} options
   *                      main_elem {[object]}      - Element to search for images/videos
   *                      data_att {[string]}       - Data attribute to search for
   *                      trans {[int]}             - Transition time (ms)
   *                      padding {[array of ints]} - Padding percentage for the open lightbox at different breakpoints [small, medium, large]
   *
   */
  constructor(options = {}){

    this.main_elem = options.main_elem || document;
    this.data_att  = options.data_att || 'lightbox';
    this.trans     = options.trans || 600;
    this.padding   = options.padding || [10, 20, 20];

    this.bindAll();

  }

  /**
   * Finds all elements with data attribute in element and binds event listener
   */
  bindAll(){

    // Get elements by targeting the data attribute inside the element
    Array.prototype.forEach.call(this.main_elem.querySelectorAll(`[data-${this.data_att}]:not([data-${this.data_att}='false'])`), (e) => {
      this.listen(e);
    });

    window.addEventListener('resize', () => {
      this.resize();
    });

  }

  /**
   * Binds click event listener to element to open lightbox
   * @param  {[DOM object]} elem - anchor element with image child - see top for examples
   */
  listen(elem){

    elem.addEventListener("click", (e) => {
      e.preventDefault();
      this.click(elem);
    });

  }

  /**
   * Handle click on image / cover - passing no src or tag will force a close
   * @param  {[DOM object]} elem - Element clicked on, eg img parent anchor
   */
  click(elem = null){

    // If animating - cancel
    if((this.container && this.container.className.search(/\banimating\b/) !== -1) || (this.video_elem && this.video_elem.className.search(/\banimating\b/) !== -1)){
      return false;
    }

    // Pass null to close
    if(elem === null){
      this.close();
      return;
    }

    // Set required data
    let video_src = (elem.getAttribute(`data-${this.data_att}`) === 'video' ? elem.getAttribute('href') : false);
    let image_tag = elem.querySelector('img');
    let image_src = (video_src !== false ? image_tag.getAttribute('src') : elem.getAttribute('href'));

    // Set video source and get dimensions - fall back to 675
    if(video_src){

      this.is_video = true;
      this.video_src = video_src;

      let video_dimensions = elem.getAttribute('data-video-size');
      try {

        video_dimensions = (video_dimensions ? JSON.parse(video_dimensions) : {
          width: 1200,
          height: 675
        });

      } catch (e) {

        video_dimensions = {
          width: 1200,
          height: 675
        };

      }

      this.video_dimensions = video_dimensions;

    }

    // Check everything has been created
    this.cover     = this.createCover();
    this.container = this.createContainer();

    if(this.container.className.search(/\bactive\b/) === -1 && image_tag !== false && image_src !== false){

      this.image_tag = image_tag;
      this.open(image_src);

    } else {
      this.close();
    }

  }

  /**
   * Opens lightbox by loading new image from src, positioning and transitioning
   * @param  {[string]} image_src - url
   */
  open(image_src){

    // Set the container to the coordinates of the image tag - then set the optimum transform
    this.setElemToOptimum(this.container, true);

    // Set parent of image tag to loading state
    this.image_tag.parentElement.setAttribute('data-loading', '');

    // Load new image with the image tag src - src attached below callbacks
    let image = new Image();

    image.onload = () => {

      // Set loaded image for use in getOptimumTransformProperties
      this.loaded_image = image;
      this.container.style.backgroundImage = `url('${this.loaded_image.src}')`;

      // Hide image tag
      this.image_tag.style.visibility = 'hidden';

      this.container.classList.add('active');
      this.cover.classList.add('active');

      this.animate(true);

      this.image_tag.parentElement.removeAttribute('data-loading');

      image = null;

    };

    image.error = () => {

      this.image_tag.parentElement.removeAttribute('data-loading');
      this.closeFinished();

    };

    if(this.is_video){

      //https://developers.google.com/youtube/player_parameters#Parameters
      this.iframe = youtubeEmbedData(this.video_src, {
        controls: 1,
        disablekb: 0
      });

      image.src = this.iframe.thumbnail;

    } else {

      image.src = image_src;

    }

  }

  /**
   * Callback when opening lightbox has finished - set open state attributes
   */
  openFinished(){

    // Set state variable
    this.container.classList.remove('animating');
    this.container.setAttribute('data-state', 'open');

    if(this.is_video){
      this.openYoutube();
    }

  }

  /**
   * Creates an iframe, and a transitionPhase element for it, then controls animations with callbacks
   */
  openYoutube(){

    this.video_elem = document.createElement("iframe");
    this.video_elem.setAttribute('class', 'lightbox player');
    this.video_elem.setAttribute('frameborder', '0');
    this.video_elem.src = this.iframe.iframe;
    this.video_elem.onload = onPlayerReady.apply(this);

    this.positionVideo();
    document.body.appendChild(this.video_elem);

    // The API will call this function when the video player is ready.
    // Transition forward (video is played once transitioned in)
    function onPlayerReady(){
      fade('in', this.video_elem, this.trans / 2, 'ease', () => {

        // Player transitioned in - hide image behind incase pixels are slightly off
        this.container.classList.add('hide--image');

      });
    }

  }

  /**
   * Closes lightbox, if video closes video first
   */
  close(){

    // If video then reverse the youtube transition, call back will handle removing element, video src and then calling this again
    if(this.is_video){
      this.container.classList.remove('hide--image');
      fade('out', this.video_elem, this.trans / 2, 'ease', () => {

        // Player transitioned out
        this.closeYoutube();
        this.close();

      });
      return;
    }

    // Set container to optimum incase page has scrolled - this fill reset the transform from the actual image
    this.setElemToOptimum(this.container, true);

    // Removed open attribute
    this.container.setAttribute('data-state', '');

    this.container.classList.remove('active');
    this.cover.classList.remove('active');

    this.animate(false);

  }

  closeYoutube(){

    // Set is video to null so the next close() closes lightbox not player
    this.is_video = null;
    this.video_elem.parentNode.removeChild(this.video_elem);
    this.video_elem = null;

  }

  /**
   * Garbage collection of class properties
   */
  closeFinished(){

    this.image_tag.style.visibility = 'visible';
    this.cover.parentNode.removeChild(this.cover);
    this.cover = null;
    this.container.parentNode.removeChild(this.container);
    this.container = null;
    this.loaded_image = null;
    this.image_tag = null;
    this.transition = null;
    this.video_src = null;
    this.video_dimensions = null;
    this.youtube_transition = null;

  }

  /**
   * Window resize event - sets element to optimum - positions video if present
   */
  resize(){

    if(this.container && this.container.getAttribute('data-state') === 'open'){

      // Set the created image to the coordinates of the image tax - then set the optimum transform
      this.setElemToOptimum(this.container);

    }

    if(this.video_elem){
      this.positionVideo();
    }

  }

  /**
   * Works out the optimum translate and scale to transform image to fill window with max width and height centered FROM the static image bounding rect on page
   */
  getOptimumTransformProperties(){

    // Detect breakpoint and pull padding from array - convert to decimal
    let padding;
    switch(true){
      case window.innerWidth <= 640:
        padding = this.padding[0] / 100;
        break;
      case window.innerWidth <= 1024:
        padding = this.padding[1] / 100;
        break;
      default:
        padding = this.padding[2] / 100;
        break;
    }

    // Get maxium window dimensions with padding
    let window_width  = window.getComputedStyle(this.cover).width.replace('px', ''),
        window_height = window.getComputedStyle(this.cover).height.replace('px', '');

    // Get maxium window dimensions with padding
    let window_width_p  = window_width - (window_width * padding),
        window_height_p = window_height - (window_height * padding);

    // Create view dimensions we are traveling to - if video then use 16:9 - if image use natrual dimensions
    let natural_width  = this.video_src ? this.video_dimensions.width : this.loaded_image.naturalWidth,
        natural_height = this.video_src ? this.video_dimensions.height : this.loaded_image.naturalHeight;

    // Work out the new dimensions to set the element before transforming
    let width  = this.idle_coordinates.width,
        height = this.idle_coordinates.height,
        ratio  = natural_width / natural_height;

    if(natural_width >= natural_height){
      // Manipulate width
      width = (this.idle_coordinates.width * ratio);
      height = (width / ratio);
    } else {
      // Manipulate height
      height = (this.idle_coordinates.height / ratio);
      width = (height * ratio);
    }

    // Get the maximum scales (how much larger the image can go on both axis) - if natural dimensions are bigger than window then use the natural for scale
    let scale_x = (natural_width >= window_width_p) ? window_width_p / width : natural_width / width,
        scale_y = (natural_height >= window_height_p) ? window_height_p / height : natural_height / height;

    // Work out the minimum we must scale this image
    let minimum_scale = Math.min(scale_x, scale_y);

    // Get distance the image has to travel to meet the center of the window by deducting the center of the image
    let trans_x = (window_width / 2) - (this.idle_coordinates.left + (width / 2)),
        trans_y = (window_height / 2) - (this.idle_coordinates.top + (height / 2));

    return {
      width: width,
      height: height,
      trans_x: trans_x,
      trans_y: trans_y,
      scale: minimum_scale
    };

  }

  //
  /**
   * Sets element to optimum max size in viewport - calculates above
   * @param {[DOM object]}  elem
   * @param {Boolean}       animating - only need this to be true on the open, this will set the element to the image size, force repaint then set to optimum for transition
   */
  setElemToOptimum(elem, animating = false){

    this.idle_coordinates = this.image_tag.getBoundingClientRect();

    if(animating){

      elem.style.position = 'fixed';
      elem.style.top      = this.idle_coordinates.top + 'px';
      elem.style.right    = this.idle_coordinates.right + 'px';
      elem.style.bottom   = this.idle_coordinates.bottom + 'px';
      elem.style.left     = this.idle_coordinates.left + 'px';
      elem.style.width    = this.idle_coordinates.width + 'px';
      elem.style.height   = this.idle_coordinates.height + 'px';

    } else {

      let optimum_prime = this.getOptimumTransformProperties();

      elem.style.position  = 'fixed';
      elem.style.top       = this.idle_coordinates.top + 'px';
      elem.style.right     = this.idle_coordinates.right + 'px';
      elem.style.bottom    = this.idle_coordinates.bottom + 'px';
      elem.style.left      = this.idle_coordinates.left + 'px';
      elem.style.width     = optimum_prime.width + 'px';
      elem.style.height    = optimum_prime.height + 'px';
      elem.style.transform = `translate(${optimum_prime.trans_x}px, ${optimum_prime.trans_y}px) scale(${optimum_prime.scale})`;

    }

  }

  animate(forward){

    this.container.classList.add('animating');

    let optimum_prime = this.getOptimumTransformProperties(),
        start         = Date.now(),
        duration      = this.trans,
        target        = this.container;

    let current_width   = forward ? this.idle_coordinates.width : optimum_prime.width,
        to_width        = forward ? optimum_prime.width : this.idle_coordinates.width,
        from_width      = current_width;

    let current_height  = forward ? this.idle_coordinates.height : optimum_prime.height,
        to_height       = forward ? optimum_prime.height : this.idle_coordinates.height,
        from_height     = current_height;

    let current_trans_x = forward ? 0 : optimum_prime.trans_x,
        to_trans_x      = forward ? optimum_prime.trans_x : 0,
        from_trans_x    = current_trans_x;

    let current_trans_y = forward ? 0 : optimum_prime.trans_y,
        to_trans_y      = forward ? optimum_prime.trans_y : 0,
        from_trans_y    = current_trans_y;

    let current_scale   = forward ? 1 : optimum_prime.scale,
        to_scale        = forward ? optimum_prime.scale : 1,
        from_scale      = current_scale;

    let _t = this;

    updateAnimation();

    function updateAnimation(){

      let elapsed = Date.now() - start;
      let finished = true;

      if((to_width < from_width && current_width > to_width) || (to_width > from_width && current_width < to_width)){
        current_width = Math.easeOut(elapsed, from_width, to_width - from_width, duration);
        finished = false;
      }

      if((to_height < from_height && current_height > to_height) || (to_height > from_height && current_height < to_height)){
        current_height = Math.easeOut(elapsed, from_height, to_height - from_height, duration);
        finished = false;
      }

      if((to_trans_x < from_trans_x && current_trans_x > to_trans_x) || (to_trans_x > from_trans_x && current_trans_x < to_trans_x)){
        current_trans_x = Math.easeOut(elapsed, from_trans_x, to_trans_x - from_trans_x, duration);
        finished = false;
      }

      if((to_trans_y < from_trans_y && current_trans_y > to_trans_y) || (to_trans_y > from_trans_y && current_trans_y < to_trans_y)){
        current_trans_y = Math.easeOut(elapsed, from_trans_y, to_trans_y - from_trans_y, duration);
        finished = false;
      }

      if((to_scale < from_scale && current_scale > to_scale) || (to_scale > from_scale && current_scale < to_scale)){
        current_scale = Math.easeOut(elapsed, from_scale, to_scale - from_scale, duration);
        finished = false;
      }

      target.style.width = `${current_width}px`;
      target.style.height = `${current_height}px`;
      target.style.transform = `translate(${current_trans_x}px, ${current_trans_y}px) scale(${current_scale})`;

      if(!finished){

        requestAnimationFrame(updateAnimation);

      } else if(_t.container && _t.container.className.search(/\bactive\b/) !== -1){

        _t.openFinished();

      } else {

        _t.closeFinished();

      }

    }

  }

  /**
   * Set player to fixed position where currently displayed image is
   * Image is currently being transformed so cannot append as child as throws the player controls off - must be actual size
   * Padded out by 2 px all round so the transition is much smoother from the thumbnail
   */
  positionVideo(){

    let cont_rect = this.container.getBoundingClientRect();
    this.video_elem.style.top = (cont_rect.top - 2) + 'px';
    this.video_elem.style.right = (cont_rect.right - 2) + 'px';
    this.video_elem.style.bottom = (cont_rect.bottom - 2) + 'px';
    this.video_elem.style.left = (cont_rect.left - 2) + 'px';
    this.video_elem.style.width = (cont_rect.width + 4) + 'px';
    this.video_elem.style.height = (cont_rect.height + 4) + 'px';

  }

  /**
   * Appends container to the body for all child elements of lightbox - If container does not exist, creates it
   * @return {[DOM object]} container elem
   */
  createContainer(){

    if(!this.container){

      this.container = document.createElement('div');
      this.container.setAttribute('class', 'lightbox container');
      this.container.addEventListener('click', () => {
        this.click();
      });
      document.body.appendChild(this.container);
      console.log("this.container", this.container);

    }

    return this.container;
  }

  /**
   * Appends cover element to body to disallow click to other lightbox while transitioning
   * @return {[DOM object]} cover elem
   */
  createCover(){

    if(!this.cover){
      this.cover = document.createElement('div');
      this.cover.setAttribute('class', 'lightbox cover');
      this.cover.addEventListener('click', () => {
        this.click();
      });
      document.body.appendChild(this.cover);
      console.log("this.cover", this.cover);
    }

    return this.cover;

  }

}