/**
 * Require main index
 */
const Lightbox = require('./index.js');

/**
 * Require Dev assets
 */
require('./scss/main.dev.scss');

module.exports = () => {
  // Dev setup
  new Lightbox;
};