/**
 * Dev scss
 */
require('./scss/main.dev.scss');

/**
 * Dependencies
 */
import React from 'react';
import PlayPause from './index';
import YouTube from 'react-youtube';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this._playPause = this.playPause.bind(this);
  }

  playPause(paused){
    if(paused){
      this.state.target.playVideo();
    } else {
      this.state.target.pauseVideo();
    }
  }

  render() {
    return (
      <div>
        <YouTube
          videoId="bdnHKdb-Oss"
          className="youtube"
          opts={{
            controls: 0,
            modestbranding: 0,
            showinfo: 0,
            showinfo: 0,
            width: '100%',
            height: '100%',
          }}
          onReady={e => {
            this.setState({
              ready: true,
              target: e.target,
            });
          }}
        />
        {this.state.ready &&
          <PlayPause
            is_playing={false}
            // animStart={this._playPause}
            animComplete={this._playPause}
          />
        }
      </div>
    );
  }
}