const transitionMap = require('../../../utils/animation/transition-map');

/**
 * Dependencies
 */
import React from 'react';
import PropTypes from 'prop-types';


/**
 * PlayPause
 */
class PlayPause extends React.Component{

  constructor(props){
    super(props);

    if(props.is_playing){
      this.map_from = this.pauseCoord;
      this.map_to = this.playCoord;
    } else {
      this.map_from = this.playCoord;
      this.map_to = this.pauseCoord;
    }

    this.state = {
      current: this.map_from,
      paused: !props.is_playing,
    };

    this._playPause = this.playPause.bind(this);
  }


  playPause(){
    if(this.state.animating){ return; }

    this.props.animStart && this.props.animStart(this.state.paused);

    transitionMap({
      from: this.state.paused ? this.map_from : this.map_to,
      to: this.state.paused ? this.map_to : this.map_from,
      duration: 300,
      easing: 'easeOut',
      on_update: current => {
        this.setState({
          ...this.state,
          current: current,
          animating: true,
        })
      },
      on_complete: () => {
        this.props.animComplete && this.props.animComplete(this.state.paused);

        this.setState({
          ...this.state,
          animating: false,
          paused: !this.state.paused,
        })
      },
    });
  }


  get playCoord(){
    return [
      0,0,
      50,25,
      50,75,
      0,100,
      50,25,
      100,50,
      100,50,
      50,75,
    ];
  }


  get pauseCoord(){
    return [
      0,0,
      40,0,
      40,100,
      0,100,
      60,0,
      100,0,
      100,100,
      60,100,
    ];
  }


  render(){
    let c = this.state.current;

    return (
      <div className="play-pause-wrap">
        <svg
          viewbox="0 0 100 100"
          onClick={this._playPause}
          className="play-pause"
        >
          <path d={`M${c[0]},${c[1]} L${c[2]},${c[3]} L${c[4]},${c[5]} L${c[6]},${c[7]} Z M${c[8]},${c[9]} L${c[10]},${c[11]} L${c[12]},${c[13]} L${c[14]},${c[15]} Z`}/>
        </svg>
      </div>
    );
  }

}

PlayPause.propTypes = {
  is_paused: PropTypes.bool,
  animStart: PropTypes.func,
  animComplete: PropTypes.func,
};


export default PlayPause;