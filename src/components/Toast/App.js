/**
 * Require main index
 * @type {[type]}
 */
const Toast = require('./index.js');

/**
 * Dev
 */
const lorem = require('../../utils/dev/lorem');

module.exports = () => {

  // Cleanup for module reload
  if(window.Toast){
    window.Toast.stage.parentElement.removeChild(window.Toast.stage);
  }

  // Create Toast
  const Toast = require('./components/Toast');
  new Toast({
    position: 'top left',
  }, true);

  // Push 10 toasts on reload
  for(let x = 0; x <= 10; x++){
    // Toast.push(`TEST MESSAGE (${x})`, 'alert', true, (Math.random() * (7000 - 1000) + 1000));
    window.Toast.push(lorem.getRandomLorem(), 'success', true, (Math.random() * (7000 - 1000) + 1000), true);
  }
};