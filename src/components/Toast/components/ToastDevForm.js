require('../scss/main.dev.scss');

const lorem = require('../../../utils/dev/lorem');

module.exports = class ToastDevForm{

  constructor(){
    this.form = document.querySelector('.toast-dev-form');
    this.form.querySelector('button').addEventListener('click', this.dispatch.bind(this));
    this.form.querySelector(`[name='position']`).addEventListener('change', this.typeChange.bind(this));
  }

  dispatch(){
    window.Toast.push(
      this.message,
      this.type,
      true,
      this.life,
      this.dismissible
    );
  }

  typeChange(){
    window.Toast.stage.setAttribute('class', `toast__stage ${this.position}`);
  }

  get message(){
    return this.form.querySelector(`[name='message']`).value || lorem.getRandomLorem();
  }

  get type(){
    return this.form.querySelector(`[name='type']`).options[this.form.querySelector(`[name='type']`).selectedIndex].value || 'info';
  }

  get life(){
    return this.form.querySelector(`[name='life']`).value || -1;
  }

  get dismissible(){
    return this.form.querySelector(`[name='dismissible']`).options[this.form.querySelector(`[name='dismissible']`).selectedIndex].value === 'yes' ? true : false;
  }

  get position(){
    return this.form.querySelector(`[name='position']`).options[this.form.querySelector(`[name='position']`).selectedIndex].value || '';
  }

}