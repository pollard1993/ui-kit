const render = require('../../../utils/dom/render.js');
const fade   = require('../../../utils/animation/fade');
const slide  = require('../../../utils/animation/slide');

module.exports = class Toast{

  constructor(options = {}, global = true){
    this.position = options.position || 'bottom right';

    this.createStage();
    this.queue = [];

    // Globally accessible
    if(global){
      window.Toast = this;
    }
  }

  createStage(options){
    this.stage = render({
      tag: 'div',
      attr: {
        class: `toast__stage ${this.position}`,
      },
    });

    document.body.appendChild(this.stage);
  }

  push(
    message,
    type        = 'info',
    priority    = false,
    life        = 5000, 
    dismissible = false
  ){
    let new_toast = {
      type: type,
      message: message,
      life: life,
      dismissible: dismissible,
    };
    
    if(priority){
      this.queue.unshift(new_toast);
    } else {
      this.queue.push(new_toast);
    }

    this.execute();
  }

  execute(){
    if(this.toasting){ return; }

    let next = this.getNext();
    if(!next){ return; }

    this.toasting = true;

    // Remove item from queue
    this.queue.splice(this.queue.indexOf(next), 1);

    let elem = this.newNode(next);
    this.stage.insertBefore(elem, this.stage.firstChild);

    fade('in', elem, 300, 'easeInOut'); 
    slide('down', elem, 300, 'easeInOut', () => { 
       
      let life = next.life; 
      if(life > 0){
        window.setTimeout(() => { 
          this.destroy(elem);   
        }, life);
      }
 
      this.toasting = false; 
      this.execute(); 
 
    });
  }

  destroy(elem){
    fade('out', elem, 300, 'easeInOut', () => { 
      elem.style.opacity = '0'; 
      slide('up', elem, 300, 'easeInOut', () => { 
        this.stage.removeChild(elem); 
      }); 
    }); 
  }

  newNode(data){
    let new_elem = render({
      tag: 'div',
      attr: {
        class: `toast toast--${data.type} ${data.dismissible && 'dismissible'}`,
      },
      nodes: [{
        tag: 'div',
        nodes: [{
          tag: 'p',
          html: data.message,
        }],
      }],
      css: {
        display: 'none',
        opacity: 0,
      },
    });

    if(data.dismissible){
      new_elem.addEventListener('click', () => {
        this.destroy(new_elem);
      });
    }

    return new_elem;
  }

  getNext(){
    return this.queue[0];
  }

}