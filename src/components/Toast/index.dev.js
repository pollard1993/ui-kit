const ToastDevForm = require('./components/ToastDevForm');

/**
 * Run on full page load
 */
const windowLoad = () => {
  window.removeEventListener('load', windowLoad);
  console.info('Window loaded');
  require('./App')();
}

// Load
window.addEventListener('load', windowLoad);

let domReady = () => {
  document.removeEventListener('DOMContentLoaded', domReady, false);
  console.info('Document ready');
  new ToastDevForm;
}

// Ready
document.addEventListener('DOMContentLoaded', domReady);

// Webpack Hot Module Replacement API - Comment out for production
/**/
if(module.hot){
  module.hot.accept('./App', windowLoad);
}
/**/