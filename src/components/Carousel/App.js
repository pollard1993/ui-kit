/**
 * Dependencies
 */
import React from 'react';
import PropTypes from 'prop-types';
import Carousel from './index';

/**
 * Dev
 */
import Images from '../../utils/dev/images';

export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.setState({
      images_items: Images(20).map(url => {
        return {backgroundImage: url};
      }),
      content_items: [
        {
          content: 
          <div className='carousel-item--dev'>
            <div>TEST CONTENT</div>
          </div>,
        },
        {
          content: 
          <div className='carousel-item--dev'>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
          </div>,
        },
        {
          content: 
          <div className='carousel-item--dev'>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
          </div>,
        },
        {
          content: 
          <div className='carousel-item--dev'>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
          </div>,
        },
        {
          content: 
          <div className='carousel-item--dev'>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
          </div>,
        },
        {
          content: 
          <div className='carousel-item--dev'>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
          </div>,
        },
        {
          content: 
          <div className='carousel-item--dev'>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
          </div>,
        },
        {
          content: 
          <div className='carousel-item--dev'>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
          </div>,
        },
        {
          content: 
          <div className='carousel-item--dev'>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
          </div>,
        },
        {
          content: 
          <div className='carousel-item--dev'>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
            <div>TEST CONTENT</div>
          </div>,
        }
      ]
    });
  }


  render() {
    return (
      <div>
        <Carousel items={this.state.images_items} show={6} spacing={false} />
        <Carousel items={this.state.images_items} show={4} spacing={false} />
        <Carousel items={this.state.content_items} show={4} spacing={true} />
        <Carousel items={this.state.content_items} show={4} spacing={true} align='center' />
      </div>
    );
  }
}