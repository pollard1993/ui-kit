/**
 * Dependencies
 */
import PropTypes from 'prop-types';


/**
 * CarouselItem
 */
class CarouselItem extends React.Component{

  constructor(props){
    super(props);

    this.state = {
      loaded: props.config.backgroundImage ? false : true,
    };

    // If has background image then cache it, and set loaded state to trans in
    if(props.config.backgroundImage){
      let image = new Image();
      image.onload = () => {
        this.setState({
          loaded: true,
        })
      }
      image.src = props.config.backgroundImage;
    }
  }


  render(){
    return (
      <div
        className={`carousel-item ${!this.state.loaded && 'inactive'}`}
        style={{
          backgroundImage: this.props.config.backgroundImage && `url('${this.props.config.backgroundImage}')`,
          minWidth: `${this.props.width * 100}%`,
        }}

      >
        <div>{this.props.config.content || null}</div>
      </div>
    );
  }

}

CarouselItem.propTypes = {
  config: PropTypes.object,
  width: PropTypes.number,
};


export default CarouselItem;