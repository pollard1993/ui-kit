/**
 * Dependencies
 */
const transitionEventType = require('../../../utils/animation/transition-event-type');
import CarouselItem from './CarouselItem';
import PropTypes from 'prop-types';
import ArrowNav from '../../ArrowNav';
console.log("ArrowNav", ArrowNav);

export default class Carousel extends React.Component{
  constructor(props) {
    super(props);

    this.state = {
      pos: 0,
      items: props.items,
      show: props.show,
      spacing: props.spacing,
      align: props.align,
      increment: 1 / props.show,
      transitioning: false,
    };

    this.original_show   = this.state.show;
    this._left           = this.left.bind(this);
    this._right          = this.right.bind(this);
    this.transitionEvent = transitionEventType();

    this.listen();
    this.resize();
  }

  listen(){
    this._resize = this.resize.bind(this);
    window.addEventListener('resize', this._resize);
  }

  resize(){
    switch(true){
      case document.body.clientWidth <= 426 && this.state.show !== 1:
        this.setState({
          ...this.state,
          show: 1,
          increment: 1,
        });
        break;
      case document.body.clientWidth > 426 && this.state.show === 1:
        this.setState({
          ...this.state,
          show: this.original_show,
          increment: 1 / this.original_show,
        });
        break;
    }
  }

  componentDidMount(){

    // Set transition event listener
    this.carousel.addEventListener(this.transitionEvent, (e) => {
      if(e.propertyName === 'left'){
        this.finsishedTransition();
      }
    });
  }

  componentDidUnmount(){
    window.removeEventListener('resize', this._resize);
  }

  left(){
    if(this.state.transitioning){ return; }

    // If we do - then append the first item to the end
    this.setState({
      ...this.state,
      disable_trans: true,
      transitioning: true,
      direction: 'left',
      items: [
        ...this.state.items,
        this.state.items[0]
      ],
    });
  }

  right(){
    if(this.state.transitioning){ return; }

    // Append the last item to the beginning and snap left
    this.setState({
      ...this.state,
      pos: this.state.pos - this.state.increment,
      disable_trans: true,
      transitioning: true,
      direction: 'right',
      items: [
        this.state.items[this.state.items.length - 1],
        ...this.state.items,
      ],
    });
  }

  componentDidUpdate(){
    // Set the transition on update
    if(this.state.direction && this.state.disable_trans){

      // Force repaint
      this.carousel.getBoundingClientRect();

      switch(this.state.direction){
        case 'left':
          this.setState({
            ...this.state,
            pos: this.state.pos - this.state.increment,
            disable_trans: false,
          });
          break;
        case 'right':
          this.setState({
            ...this.state,
            pos: this.state.pos + this.state.increment,
            disable_trans: false,
          });
          break;
      }
    }
  }

  finsishedTransition(){
    let items = this.state.items;

    switch(this.state.direction){
      case 'left':
        // Remove first item
        items.splice(0, 1);

        // Snap right, update items and reset direction
        this.setState({
          ...this.state,
          pos: this.state.pos + this.state.increment,
          disable_trans: true,
          items: items,
          direction: null,
          transitioning: false,
        });
        break;

      case 'right':
        // remove last item
        items.splice(-1, 1);

        // Only need to update items and reset direction
        this.setState({
          ...this.state,
          items: items,
          direction: null,
          transitioning: false,
        });
        break;
    }
  }

  render() {
    let styles = {
      left: `${this.state.pos * 100}%`,
      transition: this.state.disable_trans ? 'none' : '',
    }

    return (
      <div className='carousel-wrap'>
        <div className='carousel-container'>
          <div 
            className={`carousel ${this.state.spacing ? 'carousel--spacing' : ''} ${this.state.align || ''}`} 
            ref={(elem) => { this.carousel = elem; }} 
            style={styles}
          >
            {this.state.items.map((config, i) =>
              <CarouselItem 
                config={config} 
                key={i} 
                width={this.state.increment}
              />
            )}
          </div>
          <ArrowNav left={this._left} right={this._right}/>
        </div>
      </div>
    );
  }
}

Carousel.propTypes = {
  items: PropTypes.array,
  show: PropTypes.number,
  spacing: PropTypes.bool,
  align: PropTypes.string,
};