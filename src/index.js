require('./lib/scss/main.scss');
import App from './App.js';

const render = () => {
  ReactDOM.render(<App />, document.getElementById('root'));
}

render();

// Webpack Hot Module Replacement API - Comment out for production
/**/
if(module.hot){
  module.hot.accept('./App.js', render);
}
/**/