export default (amount = 1, query = null) => {
  var _urls = [];
  for(let i = 0; i < amount; i++){
    _urls.push(`//source.unsplash.com/random/${query ? `?${query}` : i}`);
  }
  return _urls;
}