/**
 * Checks if element is in view with buffer
 * @param  {object}  elem
 * @param  {number}  buffer
 * @return {boolean}
 */
module.exports = function inView(elem, buffer = 0){
  let rect = elem.getBoundingClientRect(),
      w_x  = window.innerWidth,
      w_y  = window.innerHeight;
  return (
    rect.top + rect.height > 0 + buffer &&
    rect.top < w_y - buffer &&
    rect.left + rect.width > 0 + buffer &&
    rect.left < w_x - buffer
  );
}