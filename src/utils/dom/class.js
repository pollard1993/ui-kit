/**
 * Add class with SVG fall back for IE
 * @param {object} el         DOM element
 * @param {string} class_name Class to add
 */
module.exports.addClass = function(el, class_name){
  if(el instanceof SVGElement && _isIe()){
    let pattern       = new RegExp(`\\s?\\b(${class_name})\\b`, `g`);
    let new_class_str = `${el.getAttribute('class').replace(pattern, '')} ${class_name}`;
    el.setAttribute('class', new_class_str.trim());
  }else{
    el.classList.add(class_name);
  }
}


/**
 * Remove class with SVG fall back for IE
 * @param {object} el         DOM element
 * @param {string} class_name Class to remove
 */
module.exports.removeClass = function(el, class_name){
  if(el instanceof SVGElement && _isIe()){
    let pattern       = new RegExp(`\\s?\\b(${class_name})\\b`, `g`);
    let new_class_str = el.getAttribute('class').replace(pattern, '');
    el.setAttribute('class', new_class_str.trim());
  }else{
    el.classList.remove(class_name);
  }
}


/**
 * Checks if element has class with SVG fall back for IE
 * @param {object} el         DOM element
 * @param {string} class_name Class to check
 */
module.exports.containsClass = function(el, class_name){
  if(el instanceof SVGElement && _isIe()){
    return el.getAttribute('class').match(new RegExp(`\\s?\\b(${class_name})\\b`, `g`));
  }else{
    el.classList.contains(class_name);
  }
}