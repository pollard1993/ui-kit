/**
 * Calculates scroll bar width and sets global
 * @return {number} Scroll bar width in pixels
 */
module.exports = function scrollbarWidth(){

  let elem_o,
      elem_i,
      x_full,
      x_scroll;

  // Create elements
  elem_o = document.createElement('div');
  elem_i = document.createElement('div');

  // Get no scroll size
  elem_o.style.visibility = 'hidden';
  elem_o.style.width      = '100px';
  document.body.appendChild(elem_o);
  x_full = elem_o.offsetWidth;

  // Get scroll size
  elem_o.style.overflow = 'scroll';
  elem_i.style.width    = '100%';
  elem_o.appendChild(elem_i);
  x_scroll = elem_i.offsetWidth;

  // Reset
  elem_o.parentNode.removeChild(elem_o);

  // Set global and return
  window.scrollbar_width = x_full - x_scroll;
  return window.scrollbar_width;

}