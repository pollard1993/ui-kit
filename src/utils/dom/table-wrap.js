/**
 * Loop tables and wrap them with '.table-wrap'
 */
module.exports = function tableWrap(){

  Array.prototype.forEach.call(document.getElementsByTagName('table'), (el) => {

    if(el.parentNode.classList.contains('table-wrap')){ return false; }

    // Create and append wrapper
    let wrap       = document.createElement('div');
    wrap.className = 'table-wrap';
    wrap           = el.parentNode.insertBefore(wrap, el.nextSibling);

    // Fill wrapper
    wrap.appendChild(el);

  });

};