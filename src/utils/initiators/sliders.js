var Slider = require('../modules/Slider');

module.exports = function sliders(){

  Array.prototype.forEach.call(document.querySelectorAll('[data-slider]'), (elem) => {
    new Slider(elem, {
      // transition_type: 'fade',
      // transition_dur: 300,
      // easing: 'linear',
      // arrows: false,
      // dots: false,
      // auto_trans: 10000
    });
  });
  
}