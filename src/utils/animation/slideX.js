/**
 * Takes nodelist|array of elems and slides them along their x axis using left|right properties
 * slider.js requirement
 * @param  {string}           direction left|right (default left)
 * @param  {nodelist|array}   elements  Dom nodes
 * @param  {number}           duration  Animation in ms
 * @param  {string}           easing    Easing timing function linear|easeIn|easeOut|easeInOut
 * @param  {function}         fn        Callback (with element as scope)
 */
module.exports = function _slideX(direction, elements, duration, easing, fn){
  var FROM,
      TO,
      START = new Date().getTime(),
      DIFF,
      modShift,
      callback;

  // Prevent propagation
  Array.prototype.forEach.call(elements, function(elem, i){

    if(elem.getAttribute('data-slide-x')){
      return false;
    }

  });

  Array.prototype.forEach.call(elements, function(elem, i){
    elem.setAttribute('data-slide-x', true);
  });

  // Easing timing function
  switch(easing){
    case 'linear':
      easing = Math.linearTween;
      break;

    case 'easeIn':
      easing = Math.easeIn;
      break;

    case 'easeInOut':
      easing = Math.easeInOut;
      break;

    default: // easeOut
      easing = Math.easeOut;
      break;
  }

  Array.prototype.forEach.call(elements, function(elem, i){

    // Directional defaults - set left before transition
    elem.FROM = elem.style.left.replace('%', '') * 1;
    elem.TO   = 0;

    switch(elem.end_pos){

      case 'left':
        elem.TO = -100;
        break;

      case 'right':
        elem.TO = 100;
        break;

    }

    elem.DIFF = elem.TO - elem.FROM;

  });

  // Duration
  if(typeof duration !== 'number'){
    duration = 300;
  }
  duration = Math.abs(duration);

  // Callback
  callback = function(){

    Array.prototype.forEach.call(elements, function(elem, i){
      elem.removeAttribute('data-slide-x');
    });

    if(typeof fn === 'function'){
      fn.call(elements);
    }

  };

  // Catch last item to finish
  var finished = 0;

  // Modifier
  modShift = function(){

    var ret = false;

    Array.prototype.forEach.call(elements, function(elem, i){

      // Update values
      var CURRENT = new Date().getTime() - START,
          UPDATE  = easing(CURRENT, elem.FROM, elem.DIFF, duration);

      elem.style.left = UPDATE + '%';

      var distance = false;

      switch(direction){

        case 'left':
          distance = (UPDATE >= elem.TO);
          break;

        default:
          distance = (UPDATE <= elem.TO);
          break;

      }

      if(distance || CURRENT >= duration){

        elem.style.left = elem.TO + '%';

        finished++;

        if(finished === elements.length){

          callback();
          ret = true;

        }

      }

    });

    if(ret === false){

      requestAnimationFrame(modShift);

    }

  };

  // Trigger
  modShift();

}