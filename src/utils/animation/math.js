/**
 * Linear progression
 * @param  {number} t Time elapsed
 * @param  {number} b Starting value
 * @param  {number} c Start end difference
 * @param  {number} d Duration
 * @return {number}   Delta updated value
 */
Math.linearTween = function(t, b, c, d){

  return c*t/d + b;

};


/**
 * Cubic ease-in progression
 * @param  {number} t Time elapsed
 * @param  {number} b Starting value
 * @param  {number} c Start end difference
 * @param  {number} d Duration
 * @return {number}   Delta updated value
 */
Math.easeIn = function(t, b, c, d){

  t /= d;
  return c*t*t*t + b;

};


/**
 * Cubic ease-out progression
 * @param  {number} t Time elapsed
 * @param  {number} b Starting value
 * @param  {number} c Start end difference
 * @param  {number} d Duration
 * @return {number}   Delta updated value
 */
Math.easeOut = function(t, b, c, d){

  t /= d;
  t--;
  return c*(t*t*t + 1) + b;

};


/**
 * Cubic ease-in-out progression
 * @param  {number} t Time elapsed
 * @param  {number} b Starting value
 * @param  {number} c Start end difference
 * @param  {number} d Duration
 * @return {number}   Delta updated value
 */
Math.easeInOut = function(t, b, c, d){

  t /= d/2;

  if (t < 1){

    return c/2*t*t*t + b;

  }

  t -= 2;
  return c/2*(t*t*t + 2) + b;

};