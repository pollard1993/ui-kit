require('./math');
var transition = require('./transition');

/**
 * Fade in/out with duration and callback settings
 * @param  {string}   direction in|out
 * @param  {object}   elem      DOM node
 * @param  {number}   duration  Animation in ms
 * @param  {string}   easing    Easing timing function linear|easeIn|easeOut|easeInOut
 * @param  {function} fn        Callback (with elem as scope)
 * @return {boolean}
 */
module.exports = function _fade(direction, elem, duration, easing, fn){

  // Set default opacity
  if(!elem.style.opacity){
    elem.style.opacity = (direction === 'in' ? 0 : 1);
  }

  transition({
    from: direction === 'in' ? 0 : 1,
    to: direction === 'in' ? 1 : 0,
    duration: duration,
    easing: easing,
    on_update: (current) => {
      elem.style.opacity = current;
    },
    on_complete: () => {
      
      if(direction == 'out'){
        // Hide on fade out
        elem.style.display = 'none';
      }

      if(typeof fn === 'function'){
        fn.call(elem);
      }
    }
  });

}