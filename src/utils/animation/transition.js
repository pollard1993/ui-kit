require('./math');

/**
 * Transitions a numerical to and from value with easing
 * @param  {object|array of objects} _options - {
 *   to: {number} required
 *   from: {number} required
 *   easing: {string} Easing timing function linear|easeIn|easeOut|easeInOut
 *   on_update(current): {function} - function that returns the current value
 *   on_complete(): {function} - function that returns on completion
 * }
 */
module.exports = (..._op) => {
  _op.forEach((op) => {
    op.direction   = op.from > op.to;
    op.current     = op.from;
    op.easing      = op.easing || 'linear';
    op.start       = Date.now();
    op.on_update   = op.on_update || null;
    op.on_complete = op.on_complete || null;

    let updateAnimation = () => {
      let elapsed  = Date.now() - op.start;
      let finished = true;

      if(
        elapsed < op.duration ||
        (!op.direction && op.current <= op.to) ||
        (op.direction && op.current >= op.to)
      ){

        switch(op.easing){
          case 'easeIn':
            op.current = Math.easeIn(elapsed, op.from, op.to - op.from, op.duration);
          break;
          case 'easeOut':
            op.current = Math.easeOut(elapsed, op.from, op.to - op.from, op.duration);
          break;
          case 'easeInOut':
            op.current = Math.easeInOut(elapsed, op.from, op.to - op.from, op.duration);
          break;
          default:
            op.current = Math.linearTween(elapsed, op.from, op.to - op.from, op.duration);
          break;
        }

        finished = false;
      }

      op.on_update(op.current);

      if(!finished){
        window.requestAnimationFrame(updateAnimation);
      } else {
        op.on_update(op.to);
        if(op.on_complete){
          op.on_complete();
        }
      }

    };

    updateAnimation();

  });
}