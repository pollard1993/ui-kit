require('./math');

/**
 * Scroll window to destination with duration and callback settings
 * @param  {mixed}    destination Scroll to pixel value, selector or DOM node
 * @param  {number}   duration    Animation in ms
 * @param  {string}   easing      Easing timing function linear|easeIn|easeOut|easeInOut
 * @param  {function} fn          Callback function
 * @param  {number}   buffer      Optional vertical buffer
 * @return {boolean}
 */
module.exports = function _scrollTo(destination, duration, easing, fn, buffer){

  var FROM,
      TO,
      START = new Date().getTime(),
      DIFF,
      modShift,
      callback,
      element = document.body;

  // Prevent propagation
  if(element.getAttribute('data-scrolling')){ return false; }
  element.setAttribute('data-scrolling', true);

  // Destination
  switch(typeof destination){

    case 'number':
      destination = destination;
      break;

    case 'string':
      var elem = document.querySelector(destination);
      if(!elem){ return false; }
      destination = (window.scrollY || window.pageYOffset) + elem.getBoundingClientRect().top;
      break;

    case 'object':
      if(!destination.nodeType){ return false; }
      destination = (window.scrollY || window.pageYOffset) + destination.getBoundingClientRect().top;
      break;

    default:
      return false;

  }

  // Buffer
  if(typeof buffer === 'number'){

    destination -= buffer;

  }

  // Process below max fold
  var _body   = document.body,
      _html   = document.documentElement,
      _height = Math.max(_body.scrollHeight, _body.offsetHeight, _html.clientHeight, _html.scrollHeight, _html.offsetHeight);
  destination = Math.max(0, Math.min(destination, _height - window.innerHeight));

  // Directional defaults
  FROM = (window.scrollY || window.pageYOffset);
  TO   = destination;
  DIFF = TO - FROM;

  // Duration
  if(typeof duration !== 'number'){

    duration = 1000;

  }
  duration = Math.abs(duration);

  // Easing timing function
  switch(easing){

    case 'linear':
      easing = Math.linearTween;
      break;

    case 'easeIn':
      easing = Math.easeIn;
      break;

    case 'easeInOut':
      easing = Math.easeInOut;
      break;

    default: // easeOut
      easing = Math.easeOut;
      break;

  }

  // Callback
  callback = function(){

    element.removeAttribute('data-scrolling');

    if(typeof fn === 'function'){

      fn.call(element);

    }

  };

  // Modifier
  modShift = function(){

    // Update values
    var CURRENT = new Date().getTime() - START,
        UPDATE  = easing(CURRENT, FROM, DIFF, duration);

    window.scrollTo(window.scrollX, UPDATE);

    if(UPDATE == TO || CURRENT >= duration){

      // Done
      callback();
      return true;

    }

    requestAnimationFrame(modShift);

  };

  // Trigger
  modShift();

}