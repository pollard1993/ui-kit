const transition = require('./transition');
const mapRange   = require('../data/map-range');

module.exports = options => {
  transition({
    from: 0,
    to: 1,
    duration: options.duration,
    easing: options.easing,
    on_update: (current) => {
      options.on_update(options.from.map((value, i) => {
        return mapRange(current, 0, 1, value, options.to[i]);
      }));
    },
    on_complete: () => {
      options.on_complete();
    },
  });
};