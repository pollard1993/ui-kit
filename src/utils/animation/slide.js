require('./math');
var transition = require('./transition');

/**
 * Fade in/out with duration and callback settings
 * @param  {string}   direction up|down
 * @param  {object}   elem      DOM node
 * @param  {number}   duration  Animation in ms
 * @param  {string}   easing    Easing timing function linear|easeIn|easeOut|easeInOut
 * @param  {function} fn        Callback (with elem as scope)
 * @return {boolean}
 */
module.exports = function _slide(direction, elem, duration, easing, fn){

  // Set initial css
  elem.style.display  = 'block';
  elem.style.overflow = 'hidden';
  elem.style.height   = direction === 'up' ? `${elem.scrollHeight}px` : `0px`;

  transition({
    from: direction === 'up' ? elem.scrollHeight : 0,
    to: direction === 'up' ? 0 : elem.scrollHeight,
    duration: duration,
    easing: easing,
    on_update: (current) => {
      elem.style.height = `${current}px`;
    },
    on_complete: () => {
      if(direction === 'up'){
        // Hide on slide up
        elem.style.display = 'none';
      }

      // Reset CSS
      elem.style.overflow = '';
      elem.style.height   = '';

      if(typeof fn === 'function'){
        fn.call(elem);
      }
    }
  });

}