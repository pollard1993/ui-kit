// Fetch transition type
module.exports = () => {

  // Init vars
  let t,
      el          = document.createElement('div'),
      transitions = {
        'transition': 'transitionend',
        'OTransition': 'oTransitionEnd',
        'MozTransition': 'transitionend',
        'WebkitTransition': 'webkitTransitionEnd'
      };

  // Locate applicable event
  for(t in transitions){

    if(el.style[t] !== undefined){

      return transitions[t];

    }

  }

  // Default
  return 'transitionend';

}