/**
 * Flexible AJAX request with callback
 * @param  {object} options
 *   {
 *     @param {string}   url         Target request URL (required)
 *     @param {object}   data        POST or GET data as object property:value pairs
 *     @param {boolean}  data_object Use data object as is
 *     @param {string}   method      GET or POST (default GET)
 *     @param {boolean}  async       Run as asynchronous request (default true)
 *     @param {function} modify      Modify the XMLHttpRequest object prior to send (XHR object) => {}
 *     @param {function} finish      Callback function on success (response object) => {}
 *     @param {function} error       Callback function on error (response object) => {}
 *   }
 */
module.exports = function _xhr(options){

  let XHR,
      PARAMS = '',
      callback,
      error;


  // Throw on invalid required input
  if(typeof options !== 'object'){ return false; }
  if(typeof options.url !== 'string'){ return false; }


  // Request method
  if(typeof options.method !== 'string'){

    // Set default
    options.method = 'GET';

  }else{

    // Valid request methods
    switch(options.method.toUpperCase()){

      // POST
      case 'POST':
        options.method = 'POST';
        break;

      // GET
      default:
        options.method = 'GET';

    }

  }


  // Data object to string
  if(typeof options.data === 'object' && !options.data_object){

    // Build param string
    for(let key in options.data){

      if(options.data.hasOwnProperty(key)){

        // Check for array value
        if(typeof options.data[key] === 'object' && options.data[key].length > 0){

          // Temporary array supporting key
          let _key = encodeURIComponent(key.replace(/\[\]$/, '')) + '[]';

          // Loop values
          options.data[key].forEach((v) => {

            PARAMS += '&' + _key + '=' + encodeURIComponent(v);

          });

        }else{

          // String
          PARAMS += '&' + encodeURIComponent(key) + '=' + encodeURIComponent(options.data[key]);

        }

      }

    }

    // Shift first ampersand
    PARAMS = PARAMS.replace(/^&/, '');

  }


  // Append GET query string to URL
  if(options.method == 'GET'){

    options.url += (PARAMS ? '?' + PARAMS : '');

  }


  // Finish callback if available
  if(typeof options.finish === 'function'){

    callback = (e) => { options.finish(e); };

  }else{

    callback = () => {  };

  }


  // Error callback if available
  if(typeof options.error === 'function'){

    error = (e) => { options.error(e); };

  }else{

    error = () => {  };

  }


  // Set default asynchronous
  if(typeof options.async !== 'boolean'){

    options.async = true;

  }


  // Initialize
  XHR = new XMLHttpRequest();
  XHR.open(options.method, options.url, options.async);

  // POST data headers
  if(options.method == 'POST' && !options.data_object){

    XHR.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

  }

  // Modify request object
  if(typeof options.modify === 'function'){

    options.modify(XHR);

  }


  // Execute
  XHR.onreadystatechange = () => {

    // Handle state change
    if(XHR.readyState == 4){

      // Handle status
      if(XHR.status == 200){

        // Success
        callback(XHR);

      }else{

        // Error
        error(XHR);

      }

    }

  };

  // Send
  XHR.send(options.data_object ? options.data : PARAMS);

}