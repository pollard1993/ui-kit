/**
 * Get iframe and thumbnail data from YouTube ID
 * @param  {string} video_url YouTube URL | ID
 * @param  {object} params    Optional iframe URL parameters
 * @return {object}           {iframe, url}
 */
module.exports = function youtubeEmbedData(video_url, params = null){

  // Regex the url to get the id
  let regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
  let match = video_url.match(regExp);
  let video_id = (match && match[7].length == 11) ? match[7] : false;

  // Default URL parameters
  let default_params = {
    'rel': 0,
    'showinfo': 0,
    'autoplay': 1
  };

  // Extract and merge params
  if(typeof params === 'object'){

    for(let key in params){

      if(params.hasOwnProperty(key)){

        default_params[key] = params[key];

      }

    }

  }

  // Build query string
  let params_arr = [];
  for(let key in default_params){

    if(default_params.hasOwnProperty(key)){

      params_arr[params_arr.length] = `${key}=${default_params[key]}`;

    }

  }

  // URL bases
  let url_base_thumb  = '//img.youtube.com/vi/{{ID}}/sddefault.jpg',
      url_base_iframe = `//www.youtube.com/embed/{{ID}}?${params_arr.join('&')}`;

  return {
    iframe: url_base_iframe.replace('{{ID}}', video_id),
    thumbnail: url_base_thumb.replace('{{ID}}', video_id)
  };

}