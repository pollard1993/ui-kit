/**
 * Maps an input between 2 numbers to a relational number between 2 other numbers
 * @param  {[number]} value - value you want to map
 * @param  {[number]} low1  - what the value is from
 * @param  {[number]} high1 - what the value is to
 * @param  {[number]} low2  - what the result will be from
 * @param  {[number]} high2 - what the result will be to
 * @return {[number]}       - mapped value
 *
 * mapRange(5, 0, 10, 0, 100) = 50
 * 
 */
module.exports = function mapRange(value, low1, high1, low2, high2){
  return low2 + (high2 - low2) * (value - low1) / (high1 - low1);
}