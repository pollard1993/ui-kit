/**
 * Dependencies
 */
import React from 'react';
import PropTypes from 'prop-types';
import TextBlur from './components/TextBlur';

/**
 * Dev Dependencies
 */
require('./lib/scss/main.dev.scss');
import Images from './utils/dev/images';

export default class App extends React.Component {
  constructor(props){
    super(props);

    this.items = [
      {
        text: 'Carousel',
        image: Images(1, 'Carousel')[0],
        deviation: 5,
        href: '/carousel/',
      },
      {
        text: 'Lightbox',
        image: Images(1, 'Light,box')[0],
        deviation: 5,
        href: '/lightbox/',
      },
      {
        text: 'PlayPause',
        image: Images(1, 'pause,pause')[0],
        deviation: 5,
        href: '/play-pause/',
      },
      {
        text: 'Progress',
        image: Images(1, 'progress,loader')[0],
        deviation: 5,
        href: '/progress-loader/',
      },
      {
        text: 'Toast',
        image: Images(1, 'Toast')[0],
        deviation: 5,
        href: '/toast/',
      },
    ];
  }


  render(){
    return (
      <div>
        {
          this.items.map((item, i) => {
            return(
              <a className='text-wrap' href={`${window.location.origin}${item.href}`}>
                <TextBlur
                  text={item.text}
                  image={item.image}
                  deviation={item.deviation}
                  _key={i}
                />
              </a>
            )
          })
        }
      </div>
    );
  }
}