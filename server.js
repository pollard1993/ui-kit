/* eslint no-console: 0 */

const path                 = require('path');
const express              = require('express');
const webpack              = require('webpack');
const webpackMiddleware    = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const config               = require('./webpack.config.js');

const isDeveloping = process.env.NODE_ENV !== 'production';
const port         = 3000; // isDeveloping ? 3000 : 3000;
const app          = express();

if(isDeveloping){

  // Set up default webpack middlewwhere
  const compiler = webpack(config);
  const middleware = webpackMiddleware(compiler, {
    publicPath: '/dist',
    contentBase: 'src',
    stats: {
      colors: true,
      hash: false,
      timings: true,
      chunks: false,
      chunkModules: false,
      modules: false
    }
  });

  app.use(middleware);
  app.use(webpackHotMiddleware(compiler));

  // Remove react config from configs
  config.shift();
  // Loop configs and set up express requests
  config.forEach(_config => {

    console.log('setting up config for ', _config.name)
    console.log("output path:", path.join(_config.output.path, '/index.html'));
    console.log("public path:", _config.output.publicPath);

    app.get(_config.output.publicPath, function response(req, res) {
      // Resolve path
      res.write(middleware.fileSystem.readFileSync(path.join(_config.output.path, '/index.html')));
      res.end();
    });

  });

} else {

  // Production set static files and accept all requests
  app.use(express.static(__dirname + '/dist'));
  app.get('*', function response(req, res) {
    // Fallback to base index, this won't get called if the path can be resolved naturally
    res.sendFile(path.join(__dirname, 'dist/index.html'));
  });

}

// Set up express
app.listen(port, '0.0.0.0', function onStart(err) {
  if (err) {
    console.log(err);
  }
  console.info('==> 🌎 Listening on port %s. Open up http://0.0.0.0:%s/ in your browser.', port, port);
});
