# UIKIT / COMPONENT PROTOTYPYING SUITE
###Inspired by webpack-express-boilerplate

A quick and easy way to prototype vanilla and react JS components for web and testing them in the correct enviroments.

Serves a great purpose for developing useful utilities that have been branched off into other projects.

Made public solely for portfolio purposes.

Future plans is to produce a solid UI Kit, of which can be required in any build.

`import {Carousel, ProgressLoader} from ui-kit;`

## Installation and Running
`git clone https://bitbucket.org/pollard1993/ui-kit/`

1. `cd ui-kit`
2. `npm install`
3. `npm start`
4. navigate to http://localhost:3000 in your browser of choice. The components i've made available will be listed and more added as I go.
