'use strict';

const path              = require('path');
const webpack           = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const StatsPlugin       = require('stats-webpack-plugin');

const source_path = path.join(__dirname, '/src/');
const output_path = path.join(__dirname, '/dist/');

const default_config = {
  entry: [
    'babel-polyfill',
  ],
  output: {
    path: output_path,
    filename: '[name]-[hash].min.js',
    publicPath: '/'
  },
  externals: {
    // Use external version of React
    "react": "React",
    "react-dom": "ReactDOM"
  },
  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin(),
    new ExtractTextPlugin('/css/[name]-[hash].css'),
    new webpack.optimize.UglifyJsPlugin({
      compressor: {
        warnings: false,
        screw_ie8: true
      }
    }),
    new StatsPlugin('webpack.stats.json', {
      source: false,
      modules: false
    }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV : JSON.stringify('production')
      },
      CONSTANTS: {
        UTILS_DIR: JSON.stringify(path.join(source_path, '/utils')),
      }
    })
  ],
  module: {
    rules: [{
      test: /\.jsx?$/,
      exclude: /node_modules/,
      loader: 'babel-loader',
      options: {
        presets: [
          ['es2015', {
            modules: false,
          }],
          'react',
          'babili',
        ],
        plugins: [
          'transform-remove-console',
          'transform-object-rest-spread',
        ],
      }
    }, {
      test: /\.json?$/,
      loader: 'json'
    }, {
      test: /\.scss$/,
      use: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: [{
          loader: 'css-loader', // translates CSS into CommonJS
          options: {
            sourceMap: true,
            includePaths: [source_path],
            minimize: true,
          },
        },
        {
          loader: 'resolve-url-loader',
        },
        {
          loader: 'postcss-loader', // Process with autoprefixer
          options: {
            sourceMap: true,
            includePaths: [source_path],
            plugins: (loader) => [
              require('autoprefixer')(),
            ],
          },
        },
        {
          loader: 'sass-loader', // compiles Sass to CSS
          options: {
            sourceMap: true,
            includePaths: [source_path],
          }
        }]
      })
    }, {
      test: /\.(woff|woff2|eot|ttf|svg)$/,
      loader: 'file-loader',
      options: {
        name: 'fonts/[name].[ext]',
      },
    }]
  },
  "resolve": {
    "alias": {
      'preact-compat': 'preact-compat/dist/preact-compat',
      "react": "preact-compat",
      "react-dom": "preact-compat"
    }
  }
};

/**
 * Create config to transpile react file
 */
const react_config = Object.assign({}, default_config, {
  name: 'react',
  entry: [
    path.join(source_path, '/react.js')
  ],
  output:
    Object.assign({}, default_config.output, {
      filename: 'react.js',
    }),
  externals: {}, // Remove externals, so react is compiled
});

/**
 * Base config for src
 */
const base_config = Object.assign({}, default_config, {
  name: 'base',
  entry: [
    ...default_config.entry,
    path.join(source_path, '/index.js')
  ],
  plugins: [
    ...default_config.plugins,
    new HtmlWebpackPlugin({
      template: path.join(source_path, '/index.html'),
      inject: 'body',
      filename: 'index.html'
    }),
  ]
});

/**
 * Component Configs
 */
const text_blur_config = Object.assign({}, default_config, {
  name: 'text-blur', // Make sure name matches for hot reload
  entry: [
    ...default_config.entry,
    path.join(source_path, '/components/TextBlur/index.dev.js')
  ],
  output:
    Object.assign({}, default_config.output, {
      path: path.join(output_path, '/text-blur/'),
      publicPath: '/text-blur/'
    }),
  plugins: [
    ...default_config.plugins,
    new HtmlWebpackPlugin({
      template: path.join(source_path, '/Components/TextBlur/index.html'),
      inject: 'body',
      filename: 'index.html'
    }),
  ]
});

/**
 * Component Configs
 */
const carousel_config = Object.assign({}, default_config, {
  name: 'carousel', // Make sure name matches for hot reload
  entry: [
    ...default_config.entry,
    path.join(source_path, '/components/Carousel/index.dev.js')
  ],
  output:
    Object.assign({}, default_config.output, {
      path: path.join(output_path, '/carousel/'),
      publicPath: '/carousel/'
    }),
  plugins: [
    ...default_config.plugins,
    new HtmlWebpackPlugin({
      template: path.join(source_path, '/Components/Carousel/index.html'),
      inject: 'body',
      filename: 'index.html'
    }),
  ]
});

const toast_config = Object.assign({}, default_config, {
  name: 'toast', // Make sure name matches for hot reload
  entry: [
    ...default_config.entry,
    path.join(source_path, '/components/Toast/index.dev.js')
  ],
  externals: {}, // If not using react then pull it
  output:
    Object.assign({}, default_config.output, {
      path: path.join(output_path, '/toast/'),
      publicPath: '/toast/'
    }),
  plugins: [
    ...default_config.plugins,
    new HtmlWebpackPlugin({
      template: path.join(source_path, '/Components/Toast/index.html'),
      inject: 'body',
      filename: 'index.html'
    }),
  ]
});

const progress_loader_config = Object.assign({}, default_config, {
  name: 'progress-loader', // Make sure name matches for hot reload
  entry: [
    ...default_config.entry,
    path.join(source_path, '/components/ProgressLoader/index.dev.js')
  ],
  externals: {}, // If not using react then pull it
  output:
    Object.assign({}, default_config.output, {
      path: path.join(output_path, '/progress-loader/'),
      publicPath: '/progress-loader/'
    }),
  plugins: [
    ...default_config.plugins,
    new HtmlWebpackPlugin({
      template: path.join(source_path, '/Components/ProgressLoader/index.html'),
      inject: 'body',
      filename: 'index.html'
    }),
  ]
});

const play_pause_config = Object.assign({}, default_config, {
  name: 'play-pause', // Make sure name matches for hot reload
  entry: [
    ...default_config.entry,
    path.join(source_path, '/components/PlayPause/index.dev.js')
  ],
  externals: {}, // If not using react then pull it
  output:
    Object.assign({}, default_config.output, {
      path: path.join(output_path, '/play-pause/'),
      publicPath: '/play-pause/'
    }),
  plugins: [
    ...default_config.plugins,
    new HtmlWebpackPlugin({
      template: path.join(source_path, '/Components/PlayPause/index.html'),
      inject: 'body',
      filename: 'index.html'
    }),
  ]
});

const lightbox_config = Object.assign({}, default_config, {
  name: 'lightbox', // Make sure name matches for hot reload
  entry: [
    ...default_config.entry,
    path.join(source_path, '/components/Lightbox/index.dev.js')
  ],
  externals: {}, // If not using react then pull it
  output:
    Object.assign({}, default_config.output, {
      path: path.join(output_path, '/lightbox/'),
      publicPath: '/lightbox/'
    }),
  plugins: [
    ...default_config.plugins,
    new HtmlWebpackPlugin({
      template: path.join(source_path, '/Components/Lightbox/index.html'),
      inject: 'body',
      filename: 'index.html'
    }),
  ]
});

module.exports = [
  react_config,
  base_config,
  text_blur_config,
  carousel_config,
  lightbox_config,
  play_pause_config,
  progress_loader_config,
  toast_config,
];